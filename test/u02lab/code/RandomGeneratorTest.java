package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class RandomGeneratorTest {
    private final static int END_VALUE = 4;
    private SequenceGenerator rangeGenerator;
    @Before
    public void init(){
        this.rangeGenerator = new RandomGenerator(END_VALUE);
    }
    private void testAllSequence(){
        for(int i = 0 ;i< END_VALUE;i++){
            Optional<Integer> valueGiven = rangeGenerator.next();
            Assert.assertTrue(valueGiven.get() ==0 || valueGiven.get()==1);
        }
        Assert.assertFalse(this.rangeGenerator.next().isPresent());
    }
    @Test
    public void next() {
        testAllSequence();
    }

    private void consumeRange(SequenceGenerator sequenceGenerator,int start,int stop) {
        for (int i = start; i < stop; i++) {
            sequenceGenerator.next();
        }
    }

    @Test
    public void reset() {
        consumeRange(rangeGenerator,0,END_VALUE);
        Assert.assertFalse(this.rangeGenerator.next().isPresent());
        this.rangeGenerator.reset();
        testAllSequence();
    }

    @Test
    public void isOver() {
        Assert.assertFalse(this.rangeGenerator.isOver());
        consumeRange(rangeGenerator,0,END_VALUE);
        Assert.assertTrue(this.rangeGenerator.isOver());
    }

    @Test
    public void allRemaining() {
        Assert.assertEquals(END_VALUE,rangeGenerator.allRemaining().size());
        consumeRange(rangeGenerator,0,END_VALUE);
        Assert.assertEquals(0,rangeGenerator.allRemaining().size());
    }
}