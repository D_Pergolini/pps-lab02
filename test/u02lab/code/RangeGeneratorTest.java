package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class RangeGeneratorTest {
    private final static int START_VALUE = 0;
    private final static int END_VALUE = 4;
    private final static int MEDIUM_VALUE = 2;
    private SequenceGenerator rangeGenerator;

    @Before
    public void init(){
        this.rangeGenerator = new RangeGenerator(START_VALUE,END_VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongArgumentOrder(){
        rangeGenerator = new RangeGenerator(END_VALUE,START_VALUE);
    }
    @Test
    public void testNext() {
        for(int i = RangeGeneratorTest.START_VALUE; i<= RangeGeneratorTest.END_VALUE;i++){
            Assert.assertEquals(new Integer(i),
                    this.rangeGenerator.next().get());
        }
        Assert.assertFalse(this.rangeGenerator.next().isPresent());
    }


    @Test
    public void testReset() {
        consumeRange(rangeGenerator,RangeGeneratorTest.START_VALUE,RangeGeneratorTest.MEDIUM_VALUE);
        this.rangeGenerator.reset();
        Assert.assertEquals(new Integer(RangeGeneratorTest.START_VALUE),
                this.rangeGenerator.next().get());
    }

    @Test
    public void testIsOver() {
        Assert.assertFalse(this.rangeGenerator.isOver());
        consumeRange(rangeGenerator,RangeGeneratorTest.START_VALUE,RangeGeneratorTest.END_VALUE);
        Assert.assertTrue(this.rangeGenerator.isOver());
    }


    @Test
    public void testAllRemaining() {
        List<Integer> remainingNumber = IntStream.rangeClosed(RangeGeneratorTest.START_VALUE,
                                                              RangeGeneratorTest.END_VALUE).
                                                              mapToObj(x->new Integer(x)).
                                                              collect(Collectors.toList());
        Assert.assertEquals(remainingNumber,rangeGenerator.allRemaining());
        consumeRange(rangeGenerator,RangeGeneratorTest.START_VALUE,RangeGeneratorTest.END_VALUE);
        Assert.assertEquals(new LinkedList<>(),rangeGenerator.allRemaining());
    }

    private void consumeRange(SequenceGenerator sequenceGenerator,int start,int stop){
        for (int i = start; i<=stop;i++){
            sequenceGenerator.next();
        }
    }
}