package u02lab.code;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private final int sequenceSize;
    private int numGenerated = 0;
    private final Random random = new Random();
    public RandomGenerator(int n){
        this.sequenceSize = n;
    }

    @Override
    public Optional<Integer> next() {
        if(numGenerated<sequenceSize){
            this.numGenerated++;
            return Optional.of(random.nextBoolean()==true?1:0);
        }else {
            return Optional.empty();
        }
    }

    @Override
    public void reset() {
        this.numGenerated = 0;
    }

    @Override
    public boolean isOver() {
        return this.numGenerated==sequenceSize;
    }

    @Override
    public List<Integer> allRemaining() {
        return Collections.unmodifiableList(
                IntStream.range(this.numGenerated,this.sequenceSize).
                mapToObj(x-> random.nextBoolean()==true?1:0).
                collect(Collectors.toList()));
    }
}
