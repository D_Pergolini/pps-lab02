package u02lab.code;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {
    private final int startValue;
    private final int endValue;
    private int actualValue ;
    public RangeGenerator(int start, int stop){
        if(start>stop){
            throw new IllegalArgumentException();
        }
        this.startValue = start;
        this.endValue = stop;
        this.actualValue = startValue;
    }

    @Override
    public Optional<Integer> next() {
        return this.actualValue>this.endValue?
                Optional.empty():
                Optional.of(this.actualValue++);
    }

    @Override
    public void reset() {
        this.actualValue = this.startValue;
    }

    @Override
    public boolean isOver() {
        return this.actualValue>this.endValue;
    }

    @Override
    public List<Integer> allRemaining() {
        return isOver()?
                Collections.unmodifiableList(new LinkedList<>()):
                Collections.unmodifiableList(
                        IntStream.rangeClosed(this.actualValue,
                        this.endValue).
                        mapToObj(x->new Integer(x)).
                        collect(Collectors.toList()));
    }
}
